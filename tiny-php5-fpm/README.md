tiny-php5-fpm
=============

This is a tiny PHP 5.5.20 fpm-fcgi container, configured to listen on port 9000.
Composer is also included.

Modules included:
bcmath
bz2
calendar
cgi-fcgi
Core
ctype
curl
date
dba
dom
ereg
exif
fileinfo
filter
ftp
gd
gettext
gmp
hash
iconv
intl
json
libxml
mbstring
mcrypt
mysql
mysqli
openssl
pcre
PDO
pdo_mysql
pdo_pgsql
pdo_sqlite
Phar
posix
readline
Reflection
session
SimpleXML
snmp
soap
sockets
SPL
standard
tokenizer
wddx
xml
xmlreader
xmlrpc
xmlwriter
xsl
zip
zlib


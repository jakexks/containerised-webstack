tiny-nginx
==========

nginx 1.6.2 as tiny as possible, configured to serve /srv/website and proxy php requests to php5-fpm:9000.

The resulting image is under 7 MB!

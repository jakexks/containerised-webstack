#!/bin/bash
docker run -d --name "redis" -h "redis" redis
docker run -d --name "drupal-base" jakexks/drupal-base
docker run -d --name "drupal-sites" jakexks/drupal-sites
docker run -d --name "mysql" -e MYSQL_ROOT_PASSWORD=livewyer -e MYSQL_DATABASE=drupal -e MYSQL_USER=drupal -e MYSQL_PASSWORD=drupal mysql
docker run -d --name "php5-fpm" -h "php5-fpm" --link mysql:mysql --link redis:redis --volumes-from drupal-base --volumes-from drupal-sites jakexks/tiny-php5-fpm
docker run -d -p 80:80 --name "nginx" -h "nginx" --link php5-fpm:php5-fpm --volumes-from drupal-base --volumes-from drupal-sites jakexks/tiny-nginx

